@extends('operator.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/operator/home">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <h3>Data Kontainer</h3>
              </div>
            <div class="card-header">
                <a href="{{url('operator/kontainer/create')}}" class="btn btn-primary btn-sm">Insert</a>
            </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->
                <table id="data-item" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Nama Kontainer</th>
                    <th width="240px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($kontainers as $kontainer)
                  <tr>
                    <td>{{$kontainer->nama}}</td>
                    <td>
                      <form action="{{url('operator/kontainer/delete/'.$kontainer->id)}}" method="post">
                        <a class="btn btn-primary btn-sm" href="{{url('operator/kontainer/edit/'.$kontainer->id)}}">Edit</a>

                        @csrf

                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini')">Delete</button>
                      </form>
                    </td>
                  </tr>
                  </tfoot>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
