@extends('operator.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/operator/home">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3>Tracking Pengiriman</h3>
              </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->

                  <div class="card-body">
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <label>Code</label>
                                </div>
                                <div class="col-sm">
                                    <label>{{$pengiriman->id}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <label>Tanggal Pengiriman</label>
                                </div>
                                <div class="col-sm">
                                    <label>{{$pengiriman->tgl_pengiriman}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Pelabuhan Asal</label>
                              </div>
                              <div class="col-sm">
                                <label>{{$pengiriman->pelabuhan_asal->nama}}</label>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Pelabuhan Tujuan</label>
                                  </div>
                                  <div class="col-sm">
                                    <label>{{$pengiriman->pelabuhan_tujuan->nama}}</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label>Nama Barang</label>
                                    </div>
                                    <div class="col-sm">
                                        <label>{{$pengiriman->nama_barang}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label>Jenis Barang</label>
                                    </div>
                                    <div class="col-sm">
                                        <label>{{$pengiriman->jenis_barang}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Kapal</label>
                                  </div>
                                  <div class="col-sm">
                                    <label>{{$pengiriman->kapal->nama}}</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Kontainer</label>
                                  </div>
                                  <div class="col-sm">
                                    <label>{{$pengiriman->kontainer->nama}}</label>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label>Lokasi</label>
                                    </div>
                                    <div class="col-sm">
                                        <label>{{$pengiriman->kapal->pelabuhan->nama}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/operator/pengiriman" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@endsection
