@extends('operator.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/operator/home">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3>Form Data Pengiriman</h3>
              </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->
              <form method="post" action="{{url('operator/pengiriman/store')}}">

                @csrf

                  <div class="card-body">
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <label>Tanggal Pengiriman</label>
                                </div>
                                <div class="col-sm">
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="tgl_pengiriman"/>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Pelabuhan Asal</label>
                              </div>
                              <div class="col-sm">
                                <select name="asal_id" class="select2">
                                  @foreach ($pelabuhans as $pelabuhan)
                                    <option value="{{$pelabuhan->id}}">{{$pelabuhan->nama}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Pelabuhan Tujuan</label>
                                  </div>
                                  <div class="col-sm">
                                    <select name="tujuan_id" class="select2">
                                      @foreach ($pelabuhans as $pelabuhan)
                                        <option value="{{$pelabuhan->id}}">{{$pelabuhan->nama}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label>Nama Barang</label>
                                    </div>
                                    <div class="col-sm">
                                        <input type="text" name="nama_barang" value="{{old('nama_barang')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label>Jenis Barang</label>
                                    </div>
                                    <div class="col-sm">
                                        <input type="text" name="jenis_barang" value="{{old('jenis_barang')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Kapal</label>
                                  </div>
                                  <div class="col-sm">
                                    <select name="kapal_id" class="select2">
                                      @foreach ($kapals as $kapal)
                                        <option value="{{$kapal->id}}">{{$kapal->nama}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                  <div class="col-sm">
                                    <label>Kontainer</label>
                                  </div>
                                  <div class="col-sm">
                                    <select name="kontainer_id" class="select2">
                                      @foreach ($kontainers as $kontainer)
                                        <option value="{{$kontainer->id}}">{{$kontainer->nama}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                            </div>
                        </div>
                      <input type="submit" name="submit" class="btn btn-primary btn-sm">
                  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@endsection

@push('script')
<script>
$(function() {
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
})
</script>
@endpush
