@extends('operator.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/operator/home">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3>Form Data Kapal</h3>
              </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->
              <form method="post" action="{{url('operator/kapal/update/'.$kapal->id)}}">

                @csrf

                  <div class="card-body">
                      <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Nama Kapal</label>
                              </div>
                              <div class="col-sm">
                                <input type="text" name="nama" value="{{old('nama', $kapal->nama)}}">
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Lokasi</label>
                              </div>
                              <div class="col-sm">
                                <select name="lokasi_id" class="select2">
                                @foreach ($pelabuhans as $pelabuhan)
                                <option value="{{ $pelabuhan->id }}" {{ ( $pelabuhan->id == $kapal->lokasi_id) ? 'selected' : '' }}>
                                    {{ $pelabuhan->nama }}
                                </option>
                                @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                      </div>
                      <input type="submit" name="submit">
                  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@endsection
