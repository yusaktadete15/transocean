@extends('admin.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <h3>Data Pelabuhan</h3>
              </div>
            <div class="card-header">
                <a href="{{url('admin/pelabuhan/create')}}" class="btn btn-primary btn-sm">Insert</a>
            </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->
                <table id="data-item" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Nama Pelabuhan</th>
                    <th>Status</th>
                    <th width="240px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($pelabuhans as $pelabuhan)
                  <tr>
                    <td>{{$pelabuhan->nama}}</td>
                    <td>{{$pelabuhan->negara}}</td>
                    <td>
                      <form action="{{url('admin/pelabuhan/delete/'.$pelabuhan->id)}}" method="post">
                        <a class="btn btn-primary btn-sm" href="{{url('admin/pelabuhan/edit/'.$pelabuhan->id)}}">Edit</a>

                        @csrf

                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini')">Delete</button>
                      </form>
                    </td>
                  </tr>
                  </tfoot>
                  @endforeach
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
