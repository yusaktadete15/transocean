@extends('admin.layout')
@section('title')
{{$title}}
@endsection

@section('subtitle')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> {{$subtitle}} </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>
              <li class="breadcrumb-item active">  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3>Form Data User</h3>
              </div>
            <div class="card-body">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
                <div class="card card-primary">
                    <!-- /.card-body -->
          <div class="col-12">
              <!-- /.card-header -->
              <form method="post" action="{{url('admin/user/update/'.$user->id)}}">

                @csrf

                  <div class="card-body">
                      <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Nama</label>
                              </div>
                              <div class="col-sm">
                                <label>{{$user->name}}</label>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="container">
                            <div class="row">
                              <div class="col-sm">
                                <label>Email</label>
                              </div>
                              <div class="col-sm">
                                  <label>{{$user->email}}</label>
                              </div>
                            </div>
                          </div>
                      </div>
                  <div class="form-group">
                    <div class="container">
                        <div class="row">
                          <div class="col-sm">
                            <label>Role</label>
                          </div>
                          <div class="col-sm">
                            <label>{{$user->role}}</label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <a href="/admin/user" class="btn btn-primary btn-sm" >Back</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@endsection
