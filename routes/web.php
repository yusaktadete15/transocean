<?php

use App\Http\Controllers\Admin\KapalController;
use App\Http\Controllers\Admin\KontainerController;
use App\Http\Controllers\Admin\PelabuhanController;
use App\Http\Controllers\Admin\PengirimanController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Operator\KapalController as OperatorKapalController;
use App\Http\Controllers\Operator\KontainerController as OperatorKontainerController;
use App\Http\Controllers\Operator\PelabuhanController as OperatorPelabuhanController;
use App\Http\Controllers\Operator\PengirimanController as OperatorPengirimanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'page.auth.login');

Route::prefix('admin')->middleware('auth','is_admin')->group(function()
{
    Route::get('/home', function () {
        return view('admin/layout');
    });

    Route::prefix('user')->group(function()
    {
        Route::get('/', [UserController::class, 'index']);

        Route::get('/create', [UserController::class, 'create']);
        Route::post('/store', [UserController::class, 'store']);

        Route::get('/show/{id}', [UserController::class, 'show']);

        Route::get('/edit/{id}', [UserController::class, 'edit']);
        Route::post('/update/{id}', [UserController::class, 'update']);

        Route::post('/delete/{id}', [UserController::class, 'destroy']);

    });

    Route::prefix('pelabuhan')->group(function()
    {
        Route::get('/', [PelabuhanController::class, 'index']);

        Route::get('/create', [PelabuhanController::class, 'create']);
        Route::post('/store', [PelabuhanController::class, 'store']);

        Route::get('/edit/{id}', [PelabuhanController::class, 'edit']);
        Route::post('/update/{id}', [PelabuhanController::class, 'update']);

        Route::post('/delete/{id}', [PelabuhanController::class, 'destroy']);

    });

    Route::prefix('kapal')->group(function()
    {
        Route::get('/', [KapalController::class, 'index']);

        Route::get('/create', [KapalController::class, 'create']);
        Route::post('/store', [KapalController::class, 'store']);

        Route::get('/edit/{id}', [KapalController::class, 'edit']);
        Route::post('/update/{id}', [KapalController::class, 'update']);

        Route::post('/delete/{id}', [KapalController::class, 'destroy']);

    });

    Route::prefix('kontainer')->group(function()
    {
        Route::get('/', [KontainerController::class, 'index']);

        Route::get('/create', [KontainerController::class, 'create']);
        Route::post('/store', [KontainerController::class, 'store']);

        Route::get('/edit/{id}', [KontainerController::class, 'edit']);
        Route::post('/update/{id}', [KontainerController::class, 'update']);

        Route::post('/delete/{id}', [KontainerController::class, 'destroy']);

    });

    Route::prefix('pengiriman')->group(function()
    {
        Route::get('/', [PengirimanController::class, 'index']);

        Route::get('/create', [PengirimanController::class, 'create']);
        Route::post('/store', [PengirimanController::class, 'store']);

        Route::get('/edit/{id}', [PengirimanController::class, 'edit']);
        Route::post('/update/{id}', [PengirimanController::class, 'update']);

        Route::get('/show/{id}', [PengirimanController::class, 'show']);
        Route::post('/finish/{id}', [PengirimanController::class, 'finish']);

        Route::get('/track/{id}', [PengirimanController::class, 'track']);

        Route::post('/delete/{id}', [PengirimanController::class, 'destroy']);

    });
});

Route::prefix('operator')->group(function()
{
    Route::get('/home', function () {
        return view('operator/layout');
    });

    Route::prefix('pengiriman')->group(function()
    {
        Route::get('/', [OperatorPengirimanController::class, 'index']);

        Route::get('/create', [OperatorPengirimanController::class, 'create']);
        Route::post('/store', [OperatorPengirimanController::class, 'store']);

        Route::get('/edit/{id}', [OperatorPengirimanController::class, 'edit']);
        Route::post('/update/{id}', [OperatorPengirimanController::class, 'update']);

        Route::get('/show/{id}', [OperatorPengirimanController::class, 'show']);
        Route::post('/finish/{id}', [OperatorPengirimanController::class, 'finish']);

        Route::get('/track/{id}', [OperatorPengirimanController::class, 'track']);

        Route::post('/delete/{id}', [OperatorPengirimanController::class, 'destroy']);
    });

    Route::prefix('kapal')->group(function()
    {
        Route::get('/', [OperatorKapalController::class, 'index']);

        Route::get('/create', [OperatorKapalController::class, 'create']);
        Route::post('/store', [OperatorKapalController::class, 'store']);

        Route::get('/edit/{id}', [OperatorKapalController::class, 'edit']);
        Route::post('/update/{id}', [OperatorKapalController::class, 'update']);

        Route::post('/delete/{id}', [OperatorKapalController::class, 'destroy']);

    });

    Route::prefix('pelabuhan')->group(function()
    {
        Route::get('/', [OperatorPelabuhanController::class, 'index']);

        Route::get('/create', [OperatorPelabuhanController::class, 'create']);
        Route::post('/store', [OperatorPelabuhanController::class, 'store']);

        Route::get('/edit/{id}', [OperatorPelabuhanController::class, 'edit']);
        Route::post('/update/{id}', [OperatorPelabuhanController::class, 'update']);

        Route::post('/delete/{id}', [OperatorPelabuhanController::class, 'destroy']);

    });

    Route::prefix('kontainer')->group(function()
    {
        Route::get('/', [OperatorKontainerController::class, 'index']);

        Route::get('/create', [OperatorKontainerController::class, 'create']);
        Route::post('/store', [OperatorKontainerController::class, 'store']);

        Route::get('/edit/{id}', [OperatorKontainerController::class, 'edit']);
        Route::post('/update/{id}', [OperatorKontainerController::class, 'update']);

        Route::post('/delete/{id}', [OperatorKontainerController::class, 'destroy']);

    });

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
