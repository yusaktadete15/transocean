-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Jun 2021 pada 06.56
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekspor_impor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kapals`
--

CREATE TABLE `kapals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi_id` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kapals`
--

INSERT INTO `kapals` (`id`, `nama`, `lokasi_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Evergreen', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontainers`
--

CREATE TABLE `kontainers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kontainers`
--

INSERT INTO `kontainers` (`id`, `nama`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kontainer 1', NULL, NULL, NULL),
(2, 'Kontainer 2', NULL, NULL, NULL),
(3, 'Kontainer 3', NULL, NULL, NULL),
(4, 'Kontainer 4', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_11_034748_pelabuhan', 1),
(5, '2021_06_11_034802_kontainer', 1),
(6, '2021_06_11_034812_kapal', 1),
(7, '2021_06_11_034824_pengiriman', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelabuhans`
--

CREATE TABLE `pelabuhans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `negara` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pelabuhans`
--

INSERT INTO `pelabuhans` (`id`, `nama`, `negara`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tanjung Priok', 'Indonesia', NULL, NULL, NULL),
(2, 'Port of Singapore', 'Singapura', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengirimans`
--

CREATE TABLE `pengirimans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tgl_pengiriman` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asal_id` bigint(20) UNSIGNED NOT NULL,
  `tujuan_id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapal_id` bigint(20) UNSIGNED NOT NULL,
  `kontainer_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Dalam Pengiriman',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengirimans`
--

INSERT INTO `pengirimans` (`id`, `tgl_pengiriman`, `asal_id`, `tujuan_id`, `nama_barang`, `jenis_barang`, `kapal_id`, `kontainer_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(21060001, '06/13/2021', 1, 2, 'Kentang', 'Sayuran', 1, 1, 'Selesai', NULL, '2021-06-11 21:49:04', '2021-06-11 21:54:02'),
(21060002, '06/13/2021', 1, 2, 'Kentang', 'Sayuran', 1, 1, 'Dalam Pengiriman', NULL, '2021-06-11 21:49:37', '2021-06-11 21:49:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'OPERATOR',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'admin@gmail.com', NULL, '$2y$10$E.8I24wqInrzKTZBSW8WxOuehX0n4E8xGMt9IvQ5K084utn1XTLPi', 'ADMIN', NULL, NULL, NULL),
(2, 'Jhon dew', 'jhon@gmail.com', NULL, '$2y$10$ZmWANV6emWY.z1QzNOhBUeRYucaxOf.zQ4DzOS3hffOotB2Z4GSwa', 'OPERATOR', NULL, '2021-06-11 21:46:23', '2021-06-11 21:46:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `kapals`
--
ALTER TABLE `kapals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kapals_lokasi_id_foreign` (`lokasi_id`);

--
-- Indeks untuk tabel `kontainers`
--
ALTER TABLE `kontainers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pelabuhans`
--
ALTER TABLE `pelabuhans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengirimans`
--
ALTER TABLE `pengirimans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengirimans_asal_id_foreign` (`asal_id`),
  ADD KEY `pengirimans_tujuan_id_foreign` (`tujuan_id`),
  ADD KEY `pengirimans_kapal_id_foreign` (`kapal_id`),
  ADD KEY `pengirimans_kontainer_id_foreign` (`kontainer_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kapals`
--
ALTER TABLE `kapals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kontainers`
--
ALTER TABLE `kontainers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pelabuhans`
--
ALTER TABLE `pelabuhans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengirimans`
--
ALTER TABLE `pengirimans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21060003;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kapals`
--
ALTER TABLE `kapals`
  ADD CONSTRAINT `kapals_lokasi_id_foreign` FOREIGN KEY (`lokasi_id`) REFERENCES `pelabuhans` (`id`);

--
-- Ketidakleluasaan untuk tabel `pengirimans`
--
ALTER TABLE `pengirimans`
  ADD CONSTRAINT `pengirimans_asal_id_foreign` FOREIGN KEY (`asal_id`) REFERENCES `pelabuhans` (`id`),
  ADD CONSTRAINT `pengirimans_kapal_id_foreign` FOREIGN KEY (`kapal_id`) REFERENCES `kapals` (`id`),
  ADD CONSTRAINT `pengirimans_kontainer_id_foreign` FOREIGN KEY (`kontainer_id`) REFERENCES `kontainers` (`id`),
  ADD CONSTRAINT `pengirimans_tujuan_id_foreign` FOREIGN KEY (`tujuan_id`) REFERENCES `pelabuhans` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
