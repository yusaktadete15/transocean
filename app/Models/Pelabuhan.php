<?php

namespace App\Models;

use App\Models\Kapal;
use App\Models\Pengiriman;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pelabuhan extends Model
{
    use HasFactory;

    public function kapal()
    {
        return $this->hasMany(Kapal::class);
    }

    public function pengiriman()
    {
        return $this->hasMany(Pengiriman::class);
    }
}
