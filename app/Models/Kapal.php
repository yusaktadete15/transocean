<?php

namespace App\Models;

use App\Models\Pelabuhan;
use App\Models\Pengiriman;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kapal extends Model
{
    use HasFactory;

    public function pengiriman()
    {
        return $this->hasMany(Pengiriman::class);
    }

    public function pelabuhan()
    {
        return $this->belongsTo(Pelabuhan::class, 'lokasi_id');
    }
}
