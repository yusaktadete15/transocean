<?php

namespace App\Models;

use App\Models\Pengiriman;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kontainer extends Model
{
    use HasFactory;

    public function pengiriman()
    {
        return $this->hasMany(Pengiriman::class);
    }
}
