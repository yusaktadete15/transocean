<?php

namespace App\Models;

use App\Models\Kapal;
use App\Models\Kontainer;
use App\Models\Pelabuhan;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pengirimans extends Model
{
    use HasFactory;

    public function pelabuhan_asal()
    {
        return $this->belongsTo(Pelabuhan::class, 'asal_id');
    }

    public function pelabuhan_tujuan()
    {
        return $this->belongsTo(Pelabuhan::class, 'tujuan_id');
    }

    public function kontainer()
    {
        return $this->belongsTo(Kontainer::class, 'kontainer_id');
    }

    public function kapal()
    {
        return $this->belongsTo(Kapal::class, 'kapal_id');
    }

}
