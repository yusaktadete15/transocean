<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kapal;
use App\Models\Kontainer;
use App\Models\Pelabuhan;
use App\Models\Pengirimans;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pengiriman';
    	$data['pengirimans'] = Pengirimans::get();
        return view('admin/pengiriman/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pengiriman';
    	$data['pelabuhans'] = Pelabuhan::get();
        $data['kapals'] = Kapal::get();
        $data['kontainers'] = Kontainer::get();
        return view('admin/pengiriman/insert',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'tgl_pengiriman' => 'required',
            'asal_id' => 'required',
            'tujuan_id' => 'required',
            'nama_barang' => 'required',
            'jenis_barang' => 'required',
            'kapal_id' => 'required',
            'kontainer_id' => 'required',
        ]);

        $id = IdGenerator::generate(['table' => 'pengirimans','length' => 8, 'prefix' => date('ym')]);
        $tgl_pengiriman = $request->input('tgl_pengiriman');
        $asal_id = $request->input('asal_id');
        $tujuan_id = $request->input('tujuan_id');
        $nama_barang = $request->input('nama_barang');
        $jenis_barang = $request->input('jenis_barang');
        $kapal_id = $request->input('kapal_id');
        $kontainer_id = $request->input('kontainer_id');

        $pengiriman = new Pengirimans;
        $pengiriman->id = $id;
        $pengiriman->tgl_pengiriman = $tgl_pengiriman;
        $pengiriman->asal_id = $asal_id;
        $pengiriman->tujuan_id = $tujuan_id;
        $pengiriman->nama_barang = $nama_barang;
        $pengiriman->jenis_barang = $jenis_barang;
        $pengiriman->kapal_id = $kapal_id;
        $pengiriman->kontainer_id = $kontainer_id;
        $pengiriman->save();

        $status = 'Tidak Tersedia';
        $kontainer = Kontainer::find($kontainer_id);
        $kontainer->status = $status;
        $kontainer->save();

        return redirect('admin/pengiriman')->with('message','Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pengiriman';
    	$data['pengiriman'] = Pengirimans::find($id);
        return view('admin/pengiriman/show',$data);
    }

    public function finish($id)
    {
        $status = 'Selesai';

        $pengiriman = Pengirimans::find($id);
        $pengiriman->status = $status;
        $pengiriman->save();

        return redirect('admin/pengiriman');
    }

    public function track($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pengiriman';
    	$data['pengiriman'] = Pengirimans::find($id);
        return view('admin/pengiriman/track',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengiriman = Pengirimans::find($id);
        $pengiriman->delete();

        return redirect('admin/pengiriman')->with('message', 'Data berhasil dihapus');
    }
}
