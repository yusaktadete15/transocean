<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pelabuhan;
use Illuminate\Http\Request;

class PelabuhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pelabuhan';
    	$data['pelabuhans'] = Pelabuhan::get();
        return view('admin/pelabuhan/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Input Data Pelabuhan';
        return view('admin/pelabuhan/insert',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama' => 'required',
            'negara' => 'required',
        ]);

        $nama = $request->input('nama');
        $negara = $request->input('negara');

        $pelabuhan = new Pelabuhan;
        $pelabuhan->nama = $nama;
        $pelabuhan->negara = $negara;
        $pelabuhan->save();

        return redirect('admin/pelabuhan')->with('message','Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Pelabuhan';
    	$data['pelabuhan'] = Pelabuhan::find($id);
        return view('admin/pelabuhan/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'nama' => 'required',
            'negara' => 'required',
        ]);

        $nama = $request->input('nama');
        $negara = $request->input('negara');

        $pelabuhan = Pelabuhan::find($id);
        $pelabuhan->nama = $nama;
        $pelabuhan->negara = $negara;
        $pelabuhan->save();

        return redirect('admin/pelabuhan')->with('message','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelabuhan = Pelabuhan::find($id);
        $pelabuhan->delete();

        return redirect('admin/pelabuhan')->with('message','Data berhasil dihapus');
    }
}
