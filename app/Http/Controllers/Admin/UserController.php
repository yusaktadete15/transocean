<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data User';
    	$data['users'] = User::get();
        return view('admin/user/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data User';
        return view('admin/user/insert',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
        	'name' => 'required',
        	'email' => 'required',
        	'password' => 'required',
    	]);

        $password = $request->input('password');
    	$name = $request->input('name');
    	$email = $request->input('email');
    	$password = Hash::make($password);

    	$user = new User;
    	$user->name = $name;
    	$user->email = $email;
    	$user->password = $password;
    	$user->save();

    	return redirect('admin/user')->with('message','Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data User';
    	$data['user'] = User::find($id);
        return view('admin/user/show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data User';
    	$data['user'] = User::find($id);
        return view('admin/user/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
        	'name' => 'required',
        	'password' => 'required',
    	]);

        $password = $request->input('password');
    	$name = $request->input('name');
    	$password = Hash::make($password);

    	$user = User::find($id);
    	$user->name = $name;
    	$user->password = $password;
    	$user->save();

    	return redirect('admin/user')->with('message','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('admin/user')->with('message','Data berhasil dihapus');
    }
}
