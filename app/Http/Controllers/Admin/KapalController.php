<?php

namespace App\Http\Controllers\Admin;

use App\Models\Kapal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pelabuhan;

class KapalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Kapal';
    	$data['kapals'] = Kapal::get();
        return view('admin/kapal/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Input Data Kapal';
    	$data['pelabuhans'] = Pelabuhan::get();
        return view('admin/kapal/insert',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama' => 'required',
            'lokasi_id' => 'required',
        ]);

        $nama = $request->input('nama');
        $lokasi_id = $request->input('lokasi_id');

        $kapal = new Kapal;
        $kapal->nama = $nama;
        $kapal->lokasi_id = $lokasi_id;
        $kapal->save();

        return redirect('admin/kapal')->with('message','Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Edit Data Kapal';
        $data['kapal'] = Kapal::find($id);
        $data['pelabuhans'] = Pelabuhan::get();
        return view('admin/kapal/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'nama' => 'required',
            'lokasi_id' => 'required',
        ]);

        $nama = $request->input('nama');
        $lokasi_id = $request->input('lokasi_id');

        $kapal = Kapal::find($id);
        $kapal->nama = $nama;
        $kapal->lokasi_id = $lokasi_id;
        $kapal->save();

        return redirect('admin/kapal')->with('message','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kapal = Kapal::find($id);
        $kapal->delete();

        return redirect('admin/kapal')->with('message','Data berhasil dihapus');
    }
}
