<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kontainer;
use Illuminate\Http\Request;

class KontainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Kontainer';
    	$data['kontainers'] = Kontainer::get();
        return view('admin/kontainer/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Input Data Kontainer';
        return view('admin/kontainer/insert',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama' => 'required',
        ]);

        $nama = $request->input('nama');

        $kontainer = new Kontainer;
        $kontainer->nama = $nama;
        $kontainer->save();

        return redirect('admin/kontainer')->with('message','Data berhasil diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Trans Ocean | Admin';
    	$data['subtitle'] = 'Data Kontainer';
    	$data['kontainer'] = Kontainer::find($id);
        return view('admin/kontainer/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'nama' => 'required',
        ]);

        $nama = $request->input('nama');
        $status = $request->input('status');

        $kontainer = Kontainer::find($id);
        $kontainer->nama = $nama;
        $kontainer->save();

        return redirect('admin/kontainer')->with('message','Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kontainer = Kontainer::find($id);
        $kontainer->delete();

        return redirect('admin/kontainer')->with('message','Data berhasil dihapus');
    }
}
