<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Pelabuhan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelabuhans')->insert([
            'nama' => 'Tanjung Priok',
            'negara' => 'Indonesia',
        ]);

        DB::table('pelabuhans')->insert([
            'nama' => 'Port of Singapore',
            'negara' => 'Singapura',
        ]);
    }
}
