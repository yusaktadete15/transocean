<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Kontainer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kontainers')->insert([
            'nama' => 'Kontainer 1',
        ]);

        DB::table('kontainers')->insert([
            'nama' => 'Kontainer 2',
        ]);

        DB::table('kontainers')->insert([
            'nama' => 'Kontainer 3',
        ]);

        DB::table('kontainers')->insert([
            'nama' => 'Kontainer 4',
        ]);
    }
}
