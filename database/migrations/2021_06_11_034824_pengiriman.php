<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengirimans', function (Blueprint $table) {
            $table->id();
            $table->string('tgl_pengiriman');
            $table->unsignedBigInteger('asal_id');
            $table->unsignedBigInteger('tujuan_id');
            $table->string('nama_barang');
            $table->string('jenis_barang');
            $table->unsignedBigInteger('kapal_id');
            $table->unsignedBigInteger('kontainer_id');
            $table->string('status')->default('Dalam Pengiriman');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('asal_id')->references('id')->on('pelabuhans');
            $table->foreign('tujuan_id')->references('id')->on('pelabuhans');
            $table->foreign('kapal_id')->references('id')->on('kapals');
            $table->foreign('kontainer_id')->references('id')->on('kontainers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengirimans');
    }
}
